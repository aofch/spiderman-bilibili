// Copyright 2022 The spiderman-bilibili Authors. All rights reserved.

// Project: spiderman-bilibili
// IDE: GoLand
// Author: caihe.fu@qq.com
// File: main.go
// Date: 2022/8/17 11:24
// Description: 哔哩哔哩 - 数据采集

package main

func main() {
	// 获取用户列表数据
	//spiderUserList()

	// 获取视频详情数据
	spiderVideoDetail("https://www.bilibili.com/video/BV1U14y147WW")
}
